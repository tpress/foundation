<?php 
  add_theme_support( 'custom-logo' );
  add_theme_support( 'post-formats' );
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'html5', array( 
    'comment-list', 
    'comment-form', 
    'search-form', 
    'gallery',
    'caption' 
    ) );
  add_theme_support( 'title-tag' );


function nav_class($classes) {
  return ['fc-HeaderNav__Item'];
}
function sub_nav_class($classes) {
  return ['fc-HeaderNav__SubMenu'];
}


add_filter( 'nav_menu_css_class', 'nav_class', 10, 3 );
add_filter( 'nav_menu_submenu_css_class', 'sub_nav_class', 10, 3 );