<?php 

function enqueue_parsley_scripts() {
    wp_enqueue_style( 'style.css', get_template_directory_uri() .'/node_modules/facade-components-web/dist/facade-components-web.css', null, '1.0.0', 'screen' );
    wp_enqueue_script( 'main.js', get_template_directory_uri() . '/static/dist/main.js', array(), '1.0.0', true );
}

function the_site_credit() {
    echo "<a href='http:/thacker.consulting'>Site By: Rohan Thacker</a>";
}