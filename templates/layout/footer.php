<?php 
    /**
    * mono_before_main_content hook
    * @hooked page_content_wrapper
    */
    do_action('mono_after_main_content');
?>

<footer class="fc-Footer" role="contentinfo">
      <div class="box">
            <?php dynamic_sidebar( 'sidebar-footer' ); ?> 
      </div>
      <div class="box fc-txt--center">
            <div class="span--12@xs"><?php echo date('Y') . '&nbsp' . bloginfo('name') ?></div>
            <small class="fc-SiteCredit">
                  <?php the_site_credit(); ?>
            </small>
      </div> 
</footer>
<?php wp_footer(); ?>