<!doctype html>
<html lang="<?php echo get_bloginfo('language') ?>">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="<?php echo get_bloginfo('tagline') ?>">
    <meta name="author" content="<?php echo get_bloginfo('name') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <style>
      .fc-Header { max-height:var(--headerHeight); box-sizing: border-box; }
      .sub-menu {
        display:none;
        z-index:101;
      }
      .menu-item:hover .sub-menu {
        display:block;
      }
      .fc-Footer { position:relative; }
      .fc-SiteCredit { position: absolute; right:0;bottom:0; padding-bottom:0.5rem}
    </style>
  </head>
  <body <?php body_class("fc-Page") ?>>
      <div class="fc-Header" role="banner">
        <div class="fc-HeaderBrand">
          <?php 
            if ( has_custom_logo() ) {
              the_custom_logo();
            }
          ?>
        </div>
        <?php wp_nav_menu( array(
            'container' => 'nav',
            'items_wrap' => '<nav role="navigation" id="%1$s" class="fc-HeaderNav">%3$s</nav>',
            'theme_location' => 'primary-menu',
            'nav_walker' => '/inc/nav_walker.php'
        ) ) ?>
      <button class="fc-NavToggle fc-u-Hide@lg">Menu</button>
      </div>
    
    <aside class="fc-Drawer">
      <button id="fc-Drawer__close">Close</button>
      <?php dynamic_sidebar( 'sidebar-drawer' ); ?> 
    </aside>
    <?php 
    /**
    * mono_before_main_content hook
    * @hooked page_content_wrapper
    */
    do_action('mono_before_main_content');
    ?>