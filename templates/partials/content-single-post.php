<article>
	<header>
		<?php get_template_part( 'partials/content', 'post-title' ); ?>
	</header>
	<section>
		<?php get_template_part( 'partials/content', 'post-content' ); ?>
	</section>
	<footer>
		<?php get_template_part( 'partials/content', 'post-footer' ); ?>
	</footer>
</article>