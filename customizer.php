<?php
/**
 * Contains methods for customizing the theme customization screen.
 * 
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since MyTheme 1.0
 */
class MyTheme_Customize {
   /**
    * This hooks into 'customize_register' (available as of WP 3.4) and allows
    * you to add new sections and controls to the Theme Customize screen.
    * 
    * Note: To enable instant preview, we have to actually write a bit of custom
    * javascript. See live_preview() for more.
    *  
    * @see add_action('customize_register',$func)
    * @param \WP_Customize_Manager $wp_customize
    * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
    * @since MyTheme 1.0
    */
   public static function register ( $wp_customize ) {
     $wp_customize->add_panel( 'layout_panel', array(
        'priority' => 20,
        'capability' => 'edit_theme_options',
        'title' => __( 'Layout', 'textdomain' ),
        'description' => __( 'Description of what this panel does.', 'textdomain' ),
    ) );
     
    //  $wp_customize->add_panel( 'colors_panel', array(
    //     'priority' => 30,
    //     'capability' => 'edit_theme_options',
    //     'title' => __( 'Colors', 'textdomain' ),
    //     'description' => __( 'Description of what this panel does.', 'textdomain' ),
    // ) );

     $wp_customize->add_panel( 'brand_panel', array(
        'priority' => 10,
        'capability' => 'edit_theme_options',
        'title' => __( 'Brand', 'textdomain' ),
        'description' => __( 'Description of what this panel does.', 'textdomain' ),
    ) );

    $wp_customize->add_panel( 'components_panel', array(
        'priority' => 10,
        'capability' => 'edit_theme_options',
        'title' => __( 'Components', 'textdomain' ),
        'description' => __( 'Description of what this panel does.', 'textdomain' ),
    ) );


      //1. Define a new section (if desired) to the Theme Customizer
      $wp_customize->add_section( 'section_typo', 
         array(
            'title'       => __( 'Typography', 'mytheme' ), //Visible title of section
            'priority'    => 35, //Determines what order this appears in
            'capability'  => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize some example settings for MyTheme.', 'mytheme'), 
            'panel' => 'components_panel'
            //Descriptive tooltip
         ) 
      );

      $wp_customize->add_section( 'section_buttons', array(
        'title'    => __( 'Button' ),
        'priority' => 20,
        'capability'  => 'edit_theme_options', //Capability needed to tweak
        'panel' => 'components_panel'
      ) );


      $wp_customize->add_section( 'section_header', 
         array(
            'title'       => __( 'Masthead', 'mytheme' ), //Visible title of section
            'priority'    => 35, //Determines what order this appears in
            'capability'  => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize some example settings for MyTheme.', 'mytheme'), 
            'panel' => 'layout_panel'
            //Descriptive tooltip
         ) 
      );
      
      $wp_customize->add_section( 'section_main', 
         array(
            'title'       => __( 'Masthead', 'mytheme' ), //Visible title of section
            'priority'    => 35, //Determines what order this appears in
            'capability'  => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize some example settings for MyTheme.', 'mytheme'), 
            'panel' => 'layout_panel'
            //Descriptive tooltip
         ) 
      );

      $wp_customize->add_section( 'section_footer', 
         array(
            'title'       => __( 'Footer', 'mytheme' ), //Visible title of section
            'priority'    => 35, //Determines what order this appears in
            'capability'  => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize some example settings for MyTheme.', 'mytheme'), 
            'panel' => 'layout_panel'
            //Descriptive tooltip
         ) 
      );

      $wp_customize->add_section( 'colors_section', 
         array(
            'title'       => __( 'Colors', 'mytheme' ), //Visible title of section
            'priority'    => 36, //Determines what order this appears in
            'capability'  => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize some example settings for MyTheme.', 'mytheme'), 
            'panel' => 'brand_panel'
            //Descriptive tooltip
         ) 
      );

      $wp_customize->add_section( 'title_tagline', array(
        'title'    => __( 'Title and Tagline' ),
        'priority' => 20,
        'panel' => 'brand_panel'
      ) );

      $wp_customize->add_section( 'static_front_page', array(
        'title'    => __( 'Landing Page' ),
        'priority' => 30,
        'panel' => 'layout_panel'
      ) );

      $wp_customize->add_section( 'logo_favicon', array(
        'title'    => __( 'Logo and Favicon' ),
        'priority' => 20,
        'panel' => 'brand_panel'
      ) );
      
      
      
      //2. Register new settings to the WP database...
      $wp_customize->add_setting( 'display_site_credit', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => 'True', //Default setting/value to save
            'type'       => 'option', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      

      $wp_customize->add_setting( 'font_size', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '16', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
    
      $wp_customize->add_setting( 'color_background', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#555', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
      $wp_customize->add_setting( 'color_foreground', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#555', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
      $wp_customize->add_setting( 'accent_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#555', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
      $wp_customize->add_setting( 'text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#333', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
      
      $wp_customize->add_setting( 'color_link', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#555', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
     
      $wp_customize->add_setting( 'btn_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '#555', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      

      $wp_customize->add_setting( 'container_width', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '80', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
    
      // $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'Background_color_control', array(
      //     'label'      => __( 'Background', 'mytheme' ),
      //     'section'    => 'colors_section',
      //     'settings'   => 'background_color',
      //     'priority'   => 1,
      //   ) ) 
      // );
      $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'foreground_color_control', array(
          'label'      => __( 'Foreground', 'mytheme' ),
          'section'    => 'colors',
          'settings'   => 'color_foreground',
          'priority'   => 1,
        ) ) 
      );
      $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'accent_color_control', array(
          'label'      => __( 'Accent', 'mytheme' ),
          'section'    => 'colors',
          'settings'   => 'accent_color',
          'priority'   => 1,
        ) ) 
      );
      $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'text_color_control', array(
          'label'      => __( 'Text', 'mytheme' ),
          'section'    => 'colors',
          'settings'   => 'text_color',
          'priority'   => 1,
        ) ) 
      );
      $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'link_color_control', array(
          'label'      => __( 'Link', 'mytheme' ),
          'section'    => 'colors',
          'settings'   => 'color_link',
          'priority'   => 1,
        ) ) 
      );

      $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'logo', array(
        'label'      => __( 'Upload a logo', 'theme_name' ),
        'section'    => 'logo_favicon',
        'settings'   => 'custom_logo',
        'context'    => 'your_setting_context' 
        )
       )
      );
      
      $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'favicon', array(
        'label'      => __( 'Upload a logo', 'theme_name' ),
        'section'    => 'logo_favicon',
        'settings'   => 'site_icon',
        'context'    => 'your_setting_context' 
        )
       )
      );
   
      $wp_customize->remove_control("site_icon");
      $wp_customize->remove_control("custom_logo");
      
      $wp_customize->add_setting( 'copyright', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => 'All Rights Reserved - ', //Default setting/value to save
            'type'       => 'option', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );      
      $wp_customize->add_setting( 'btn_style', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => '4', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage'
            
         ) 
      );      

      $wp_customize->add_setting( 'main_container', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
         array(
            'default'    => 'TRUE', //Default setting/value to save
            'type'       => 'option', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
         ) 
      );
            
      //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
      $wp_customize->add_control('display_copyright', array(
          'label'    => __( 'Copyright', 'mytheme' ),
          'section'  => 'section_footer',
          'settings' => 'copyright',
          'type'     => 'text',
        )
      );
      
      $wp_customize->add_control('container_width_control', array(
          'label'    => __( 'Width', 'mytheme' ),
          'section'  => 'static_front_page',
          'settings' => 'container_width',
          'type'     => 'range',
          'input_attrs' => array(
            'min' => 50,
            'max' => 100,
            'step' => 2,
          ),
        )
      );
      
      $wp_customize->add_control('display_font_size', array(
          'label'    => __( 'Font Size', 'mytheme' ),
          'section'  => 'section_typo',
          'settings' => 'font_size',
          'type'     => 'number',
        )
      );

      $wp_customize->add_control('display_site_credit_control', array(
          'label'    => __( 'Display By Line', 'mytheme' ),
          'section'  => 'section_footer',
          'settings' => 'display_site_credit',
          'type'     => 'radio',
          'choices'  => array(
            True  => 'Yes',
            False => 'No',
          ),
        )
      );

      $wp_customize->add_control('display_btn_style_control', array(
          'label'    => __( 'Button Style', 'mytheme' ),
          'section'  => 'section_buttons',
          'settings' => 'btn_style',
          'type'     => 'radio',
          'choices'  => array(
            '0'  => 'Edge',
            '8' => 'Radius',
            '16' => 'Rounded',
          ),
        )
      );



      //4. We can also change built-in settings by modifying properties. For instance, let's make some stuff use live preview JS...
      $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
      $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
      $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
      $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
      $wp_customize->get_setting( 'text_color' )->transport = 'postMessage';
      $wp_customize->get_setting( 'accent_color' )->transport = 'postMessage';
      $wp_customize->get_setting( 'font_size' )->transport = 'postMessage';
      $wp_customize->get_setting( 'copyright' )->transport = 'postMessage';
      $wp_customize->get_setting( 'container_width' )->transport = 'postMessage';
   }

   /**
    * This will output the custom WordPress settings to the live theme's WP head.
    * 
    * Used by hook: 'wp_head'
    * 
    * @see add_action('wp_head',$func)
    * @since MyTheme 1.0
    */
   public static function header_output() {
      ?>
      <!--Customizer CSS--> 
      <style type="text/css">
           <?php self::generate_css('#site-title a', 'color', 'header_textcolor', '#'); ?> 
           <?php self::generate_css('body', 'background-color', 'background_color', '#'); ?> 
           <?php self::generate_css('body', 'color', 'text_color'); ?> 
           <?php self::generate_css('body', 'font-size', 'font_size', '', 'px'); ?> 
           <?php self::generate_css('a', 'color', 'link_textcolor'); ?>
           <?php self::generate_css('.btn', 'background-color', 'accent_color'); ?>
           <?php self::generate_css('.container', 'max-width', 'container_width', '', '%'); ?>
      </style> 
      <!--/Customizer CSS-->
      <?php
   }
   
   /**
    * This outputs the javascript needed to automate the live settings preview.
    * Also keep in mind that this function isn't necessary unless your settings 
    * are using 'transport'=>'postMessage' instead of the default 'transport'
    * => 'refresh'
    * 
    * Used by hook: 'customize_preview_init'
    * 
    * @see add_action('customize_preview_init',$func)
    * @since MyTheme 1.0
    */
   public static function live_preview() {
      wp_enqueue_script( 
           'mytheme-themecustomizer', // Give the script a unique ID
           get_template_directory_uri() . '/assets/js/theme-customizer.js', // Define the path to the JS file
           array(  'jquery', 'customize-preview' ), // Define dependencies
           '', // Define a version (optional) 
           true // Specify whether to put in footer (leave this true)
      );
   }

    /**
     * This will generate a line of CSS for use in header output. If the setting
     * ($mod_name) has no defined value, the CSS will not be output.
     * 
     * @uses get_theme_mod()
     * @param string $selector CSS selector
     * @param string $style The name of the CSS *property* to modify
     * @param string $mod_name The name of the 'theme_mod' option to fetch
     * @param string $prefix Optional. Anything that needs to be output before the CSS property
     * @param string $postfix Optional. Anything that needs to be output after the CSS property
     * @param bool $echo Optional. Whether to print directly to the page (default: true).
     * @return string Returns a single line of CSS with selectors and a property.
     * @since MyTheme 1.0
     */
    public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
      $return = '';
      $mod = get_theme_mod($mod_name);
      if ( ! empty( $mod ) ) {
         $return = sprintf('%s { %s:%s; }',
            $selector,
            $style,
            $prefix.$mod.$postfix
         );
         if ( $echo ) {
            echo $return;
         }
      }
      return $return;
    }
}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'MyTheme_Customize' , 'register' ) );

// Output custom CSS to live site
add_action( 'wp_head' , array( 'MyTheme_Customize' , 'header_output' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'MyTheme_Customize' , 'live_preview' ) );