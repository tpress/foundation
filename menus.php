<?php 

function parsley_navigation_menus() {

	$locations = array(
		'primary-menu' => __( 'Primary Website Menu', 'text_domain' ),
		'external-links-menu' => __( 'External Links', 'text_domain' )
	);
	register_nav_menus( $locations );

}



?>