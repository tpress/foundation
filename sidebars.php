<?php 
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Footer Sidebar', 'mono' ),
        'id' => 'sidebar-footer',
				'description' => __( 'Widgets in this area will be shown in the footer of all pages.', 'mono' ),
				'before_widget' => '<div id="%1$s" class="widget fc-span--3 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
		) );
    register_sidebar( array(
        'name' => __( 'Drawer', 'mono' ),
        'id' => 'sidebar-drawer',
				'description' => __( 'Widgets in this area will be shown in the site drawer.', 'mono' ),
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget'  => '</li>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
		) );
}
?>